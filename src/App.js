import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';


/* Components */
import Header from './components/Header';
import Create from './components/Create';
import List from './components/List';
import Edit from './components/Edit';
import Welcome from './components/Welcome'

import './App.css';


function App() {
  return (
    <>
      <Header/>
      <Router>
        <Switch>
        <Route exact path="/">
            <Welcome/>
          </Route>
          <Route exact path="/list">
            <List/>
          </Route>
          <Route exact path="/edit">
            <Edit/>
          </Route>
          <Route>
            <Create exact path="/create"/>
          </Route>
          
        </Switch>    
      </Router>
    </>
  );
}

export default App;
