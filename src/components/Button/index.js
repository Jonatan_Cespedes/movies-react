import React from 'react';

import './Button.css';

const Button = (props) => {
    return ( 
        <div >
            <button className={props.className} type={props.type}>ENVIAR</button>
        </div>
     );
}
 
export default Button