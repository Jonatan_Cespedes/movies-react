import React from 'react';
import { motion } from "framer-motion";


/* Stylesheet */
import './Welcome.css';
/* Components */
import Register from '../Register/index'

const Welcome = () => {
    
    const variants = {
        hidden: { x: -1000 },
        visible: { x: 0}
    }

    return ( 
        <>
            <motion.h1 
                initial="hidden"
                animate="visible"
                transition={{ ease: "easeOut", duration: 1 }}
                variants={variants}
                className="text-center">Bienvenid@s a <br/> MoviesRest</motion.h1>
            <Register/>
            
        </>
     );
}
 
export default Welcome;