import React, {useEffect, useState} from 'react';
import './Header.css';
import { motion } from "framer-motion";

const Header = () => {

    useEffect(() => {
        ((d) => {
            const $btnMenu = d.querySelector(".btn-menu"),
            $menu = d.querySelector(".menu");
        
            $btnMenu.addEventListener("click", e=>{
                $btnMenu.firstElementChild.classList.toggle("none");
                $btnMenu.lastElementChild.classList.toggle("none");
                $menu.classList.toggle("is-active");
            });
        
            d.addEventListener("click", e=>{
                if(!e.target.matches(".menu a"))return false;
        
                $btnMenu.firstElementChild.classList.remove("none");
                $btnMenu.lastElementChild.classList.add("none");
                $menu.classList.remove("is-active");
            })
        })(document);
    }, []);

    const variants = {
        hidden: { x: 1000 },
        visible: { x: 0}
    }
    
    return ( 
        <header className="header">
            <section className="container">
                <div className="logos">
                    <a href="/"><img id="logo1" src="logo192.png"/></a>
                </div>
                <button className="btn-menu">
                <span><i class="far fa-bars"></i></span>
                   <span className="none"><i className="fas fa-times"></i></span>
                </button>
                <nav className="menu">
                    <motion.a
                    initial="hidden"
                    animate="visible"
                    transition={{ ease: "easeOut", duration: 1 }}
                    variants={variants}
                    href="/create">Crear</motion.a>
                    <motion.a 
                    initial="hidden"
                    animate="visible"
                    transition={{ ease: "easeOut", duration: 1.5 }}
                    variants={variants}
                    href="/list">Listar</motion.a>
                    <motion.a 
                    initial="hidden"
                    animate="visible"
                    transition={{ ease: "easeOut", duration: 2 }}
                    variants={variants}href="/edit">Editar</motion.a>
                </nav>
            </section>
        </header>
     );
}
 
export default Header;