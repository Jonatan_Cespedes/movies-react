import React from 'react';
import { useForm } from "react-hook-form";
import { motion } from "framer-motion";


/* Stylesheet */
import './Register.css';
/* Components */
import Button from '../Button/index';

const Register = () => {
    
    /* React hook form */
    const { register, handleSubmit, watch, errors } = useForm();
    console.log(errors)
    const onSubmit = data => {
        let dataForm = {
            email: data.email,
            pass: data.pass
        }
        console.log(dataForm)
        let settings = {
            method: "POST",
            headers: {
                "Accept":'application/json',
                "content-type": "application/json"
            },
            body: JSON.stringify(dataForm)
        };
    
        fetch('https://movies-apirest.herokuapp.com/register', settings)
        .then(function(response) {
            return response.json();
        })
        .then(function(response) {
        console.log(response)
        })
        .catch(function(errors) {
            console.log("Error! " + errors);
        }); 
    } 
    
    /* Famer-motion */

    const variants = {
        hidden: { opacity: 0 },
        visible: { opacity: 1}
    }

    return ( 
        <motion.div 
        initial="hidden"
        animate="visible"
        transition={{ ease: "easeOut", duration: 4 }}
        variants={variants} 
        className="container p-4 d-flex justify-content-center">
            <div className="row justify-content-center shadow-lg p-3 mb-5 bg-body rounded max-w-50">
                <div className="col-12 col-md-8">
                    <h3 className="text-center">Registrate</h3>
                </div>
                <div className="col-12 col-md-9">
                    <form action="" onSubmit={handleSubmit(onSubmit)}>
                        <input 
                        className="form-control mb-3"
                        name="userName"
                        type="text"
                        placeholder="Nombre de usuario"
                        ref={register({
                            required:{
                                value: true,
                                message: "Ingrese su nombre de usuario"
                            }
                        })}
                        autoComplete="off"/>
                        <input 
                        className="form-control mb-3"  
                        name="email" 
                        type="email" 
                        placeholder="Email" 
                        ref={register({
                            required: {
                                value:true,
                                message: "Ingrese su email"
                                },
                            
                                pattern: {
                                    value:/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i,
                                    message: "Ingrese un mail válido"
                                }
                            })}/>
                        {errors.email && (
                            <span className="text-danger">
                                {errors.email.message}
                            </span>
                        )}

                        <input 
                        className="form-control mb-3"  
                        name="pass" type="password" 
                        placeholder="Contraseña" 
                        ref={register({
                            required:{
                                value: true,
                                message: "Ingrese una contaseña"
                            } 
                        })}/>
                        {errors.pass && (
                            <span className="text-danger">
                                {errors.pass.message}
                            </span>
                        )}

                        <Button 
                        className="btn btn-primary" 
                        type="submit"/>
                    </form>
                </div>
              
            </div>
           
        </motion.div> 
     );
}
 
export default Register;